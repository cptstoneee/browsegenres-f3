<footer class="mastfoot mt-auto">
  <div class="inner">
    <a href="/contact">Contact</a> <div class="footer-space">|</div> <a href="/privacy">Privacy Notice</a> <div class="footer-space">|</div> <a href="/terms">Terms of Service</a>
  </div>
</footer>
</div>
</body>
