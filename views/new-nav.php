<body class="text-center">
  <div class="cover-container d-flex w-100 min-vh-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
      <div class="inner">
        <h3 class="masthead-brand">BrowseGenres</h3>
        <nav class="nav nav-masthead justify-content-center">
          <a class="nav-link active" href="/browse">Browse</a>
          <a class="nav-link" href="/about">About</a>
          <a class="nav-link mr-4" href="/newsletter">Newsletter</a>
          <check if="{{ @loggedin }}">
            <true>
              <a class="btn btn-primary log" href="/logout">Sign out</a>
            </true>
            <false>
              <a class="btn btn-primary log" href="/login">Sign in</a>
            </false>
          </check>
        </nav>
      </div>
    </header>