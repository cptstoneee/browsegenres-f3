

<body>
  <div class="login-container">
    <div class="login">
      <form class="form-signin" method="post" action="authenticate">
      <h2 class="form-signin-heading">Please sign in</h2>
        <check if="{{ @loginError }}">
            <div class="alert alert-danger" role="alert">{{ @loginError }}</div>
          </check>  
        <label class="sr-only" for="inputUsername">Username</label>
        <input
          type="text"
          name="username"
          autofocus=""
          required=""
          placeholder="Username"
          class="form-control"
          id="inputUsername"
        />
        <label class="sr-only" for="inputPassword">Password</label>
        <input
          type="password"
          name="password"
          required=""
          placeholder="Password"
          class="form-control"
          id="inputPassword"
        />
        <button type="submit" class="btn btn-lg btn-primary btn-block">
          Sign in
        </button>
      </form>
      <div class="divider"></div>
      <a class="register" href="/register">New around here? Sign up!</a>
    </div>
  </div>
</body>
