<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <!-- <div class="results">
  <repeat group="{{ @results->artists->items?:[] }}" value="{{ @artist }}">
  <img
    class="artist"
    src="{{@artist->images['1']->url}}"
    alt="{{@artist->name}}"
    data-id="{{@artist->id}}"
    data-token="{{@results->mytoken}}"
    data-token1=""
    height="300"
    width="300"
  />
</repeat>
</div> -->


<table class="pure-table pure-table-striped">
    <!-- <thead>
        <tr>
            <th>Bild</th>
            <th>Titel</th>
            <th>Beschreibung</th>
            <th>Trainer</th>
            <th>Raum</th>
            <th>Datum</th>
            <th>Beginn</th>
            <th>Ende</th>
            <th>Details</th>
            <th>Bearbeiten</th>
            <th>Löschen</th>
        </tr>
    </thead> -->
    <tbody>
        <repeat group="{{ @results?:[] }}" value="{{ @artist }}">
            <tr>
                <td><img src="https://i.scdn.co/image/9f90d58b5f54999e50d2e14d1f0de9c3924ac0b1" alt="Smiley face" height="55" width="55"></td>
                <td><div class="artistName" id="artist-name"><p>J Balvin</p></div></td>
                <td><p class="popularity-nr" id="popularity-nr">99</p></td>
                <td><p class="followers-nr" id="followers-nr">18,023,113</p></td>
                <td><audio controls="" id="audio-track"><source src="https://p.scdn.co/mp3-preview/60e9246623919cb164490957ba79af46ceabe237?cid=309e69f9768341b4b4165121571e418c" type="audio/ogg"><source src="https://p.scdn.co/mp3-preview/60e9246623919cb164490957ba79af46ceabe237?cid=309e69f9768341b4b4165121571e418c" type="audio/mpeg"></audio></td>
                <td><a href="https://open.spotify.com/artist/3b8QkneNDz4JHKKKlLgYZg">Spotify</a></td>
                <td><button class="pure-button btn-delete" data-url="/courses/{{ @course.id }}/delete">Delete</button></td>
            </tr>
        </repeat>
    </tbody>
</table>