<link rel="stylesheet" type="text/css" href="/css/new-starting-layout.css" media="screen" />
<main role="main" class="inner cover">
  <h1 class="cover-heading">Did you know?</h1>
  <p class="lead">There are more than 4.000 genres listed on spotify!</p>
  <p class="lead">How many of them do you know?</p>
  <p class="lead font-weight-bold">Explore spotify´s universe of genres!</p>
  <p class="lead">
    <check if="{{ @loggedin }}">
      <true>
        <a href="/browse" class="btn btn-lg btn-secondary">Start browsing</a>
      </true>
      <false>
        <a href="/login" class="btn btn-lg btn-secondary">Sign in</a>
      </false>
    </check>
  </p>
</main>