<body>
    <section class="global-genre-button-section">
    <h2 class="pb-4 browsing-header">Start Browsing</h2>
  <div class="genre-buttons-group">
    <repeat group="{{ @genres }}" value="{{ @row }}">
      <tr>
        <td>
          <a
            href="/browse/{{ @row.genre_name }}"
            style="background-image: url('{{ @row.genre_img_url }}');"
            class="global-genres-btn"
            >{{ @row.genre_name }}</a
          >
        </td>
      </tr>
    </repeat>
  </div>
</section>
</body>
