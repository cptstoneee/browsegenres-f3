<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


<div class="container-fluid">

    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Your activity: {{@amountArtists}} artists found!</h1>

        <table class="table table-hover table-dark">
            <thead>
                <tr>
                    <th scope="col" ></th>
                    <th scope="col" id="headerName">Name</th>
                    <th scope="col" id="headerPopularity">Popularity</th>
                    <th scope="col" id="headerFollowers">Followers</th>
                    <th scope="col" id="headerPreview">Preview</th>
                    <th scope="col" id="headerGoto">Go to</th>
                    <th scope="col" id="headerDelete">Delete</th>
                </tr>
            </thead>
            <tbody>
                <repeat group="{{ @results?:[] }}" value="{{ @artist }}">
                    <tr>
                        <td><img class="dashboardImg" src="{{@artist['artist_image']}}" alt="{{@artist['name']}}" height="85" width="85"></td>
                        <td>
                            <div class="dashboardArtistName" id="artist-name">
                                <p>{{@artist['name']}}</p>
                            </div>
                        </td>
                        <td id="cellPopularity"><p class="dashboardPopularityNr" id="popularity-nr">{{@artist['spotify_popularity']}}</p></td>
                                <td id="cellFollowers"><p class="dashboardFollowersNr" id="followers-nr">{{@artist['spotify_followers']}}</p></td>
                        <td id="cellAudio"><audio controls="" class="dashboardAudio" id="audio-track">
                                <source src="{{@artist['artist_toptrack_preview']}}" type="audio/mpeg"></audio></td>
                        <td><a href="{{@artist['spotify_artist_external_url']}}" i class="fa fa-spotify fa_custom fa-3x" target="_blank"></i></a></td>
                        <td><a href="/deletefromcart/{{ @artist.id }}/artist" data-url="/deletefromcart/{{ @artist.id }}/artist" i class="fa fa-trash-o fa_custom fa-3x" target="_self"></i></a></td>
                        <!-- <td><button i class="fa fa-trash-o fa_custom fa-3x" data-url="/deletefromcart/{{ @artist.id }}/artist" id="btn-delete"></button></i></td> -->
                    </tr>
                </repeat>
            </tbody>
        </table>






        <!-- <h2 class="sub-header">Section title</h2> -->

    </div>
    <!-- </div> -->
</div>