<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <div class="results">
  <repeat group="{{ @results->artists->items?:[] }}" value="{{ @artist }}">
  <img
    class="artist"
    src="{{@artist->images['1']->url}}"
    alt="{{@artist->name}}"
    data-id="{{@artist->id}}"
    data-token="{{@results->mytoken}}"
    data-token1=""
    height="300"
    width="300"
  />
</repeat>
</div>

<repeat group="{{ @jScripts }}" value="{{ @script }}">
  <script src="{{ @script }}"></script>

  <div
    class="modal"
    tabindex="-1"
    role="dialog"
    id="myModal"
    data-toggle="modal"
  >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modalContainer">
          <div class="modal-body" id="getCode">
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <div class="imgKpiContainer" id="img-kpi-container">
              <div class="artistImg" id="artist-img"></div>
              <div class="kpiContainer" id="kpi-container">
                <div class="popularityNr" id="popularity-nr"></div>
                <div class="popularityLabel" id="popularity-label"></div>
                <div class="followersNr" id="followers-nr"></div>
                <div class="followersLabel" id="followers-label"></div>
              </div>
            </div>
            <div class="artistName" id="artist-name"></div>
            <div class="genreContainer" id="genre-container">
              <div class="genreNames" id="genre-names"></div>
            </div>
            <div class="song" id="song-label"></div>
            <p id="getCodeText"></p>
            <div class="modal-footer">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div></repeat
>
