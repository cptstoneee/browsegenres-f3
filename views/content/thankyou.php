<link rel="stylesheet" type="text/css" href="/css/new-starting-layout.css" media="screen" />
<main role="main" class="inner cover">
  <h1 class="cover-heading">Thank you for registering.</h1>
  <p class="lead">We have sent a verification email to the address provided.</p>
  <img src="/css/images/email.png" alt="">
</main>