<include href="/views/modules/alert.html" />

<body>
  <div class="register-container">
    <div class="register">
      <form class="form-signup" method="post" action="register">
        <h2 class="form-signin-heading">Please sign up</h2>

        <label class="sr-only" for="inputUsername">Username</label>
        <check if="{{ @errors.username }}">
          <div class="field-error">{{ @errors.username }}</div>
        </check>

        <input
          type="text"
          name="username"
          autofocus=""
          placeholder="Username"
          class="form-control"
          id="inputUsername"
        />

        <label class="sr-only" for="inputPassword">Password</label>
        <check if="{{ @errors.password }}">
          <div class="field-error">{{ @errors.password }}</div>
        </check>

        <input
          type="password"
          name="password"
          placeholder="Password"
          class="form-control"
          id="inputPassword"
        />

        <label class="sr-only" for="inputEmail">Email address</label>
        <check if="{{ @errors.email }}">
          <div class="field-error">{{ @errors.email }}</div>
        </check>

        <input
          type="text"
          name="email"
          autofocus=""
          placeholder="Email address"
          class="form-control"
          id="inputEmail"
        />

        <button type="submit" class="btn btn-lg btn-primary btn-block">
          Sign up
        </button>
      </form>
    </div>
  </div>
</body>
