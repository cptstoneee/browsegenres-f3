
<body>
    <p>It looks like you haven't discovered any artists yet. Click on the button and start browsing. Whenever you click on an artist and genre, you will find them later here on your dashboard.</p>
    <a href="/browse" class="btn btn-lg btn-secondary">Start browsing</a>
</body>
