<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Browsegenres.com | {{ @pageTitle }}</title>
    <!-- <link rel="stylesheet" type="text/css" href="/css/layout.css" /> -->
    
    <!-- jQuery -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --> 
    <script src="/js/jquery-3.4.1.min.js"></script>
    
    <!-- jQuery UI -->
    <script src="/js/jquery-ui.min.js"></script>
    <link href="/css/jquery-ui.min.css" rel="stylesheet">
    <link href="/css/theme.css" rel="stylesheet">
    
    <!-- Bootstrap Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <script src="/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="/css/dashboard.css"> -->
    <link rel="stylesheet" href="/css/new-layout.css">
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
  </head>



