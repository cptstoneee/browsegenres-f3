<?php
namespace Controller;
use \Template;
use \App\SystemHelper as SH;

class NewsletterController extends \App\LogChecker {
// class NewsletterController {
    public function index($f3, $params) {
        // https://fatfreeframework.com/3.6/framework-variables
        // Diese drei Variablen müssen immer gesetzt werden

        $f3->set('content', '/views/content/newsletter.php');

        // Template ausgeben
        echo Template::instance()->render('/views/new-layout.php');
    }
}