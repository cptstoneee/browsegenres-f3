<?php

namespace Controller;

use \Template;

class TokenController extends \App\LogChecker
{
    // class AboutController {
    /**
     * 
     */
    public function gettoken()
    {

        // we need a valid token for the genre query
        $tokenNeeded = new \App\SpotifyTokenManager();
        // check db for the last saved entry
        // (by asking if the current timestamp is < than the expiration time of the saved token)
        // if this valuets to true the db returns the valid token
        // otherwise we know that we have to request a new token
        $checkForToken = $tokenNeeded->getLastSavedToken();
        // var_dump($checkForToken);
        // exit();

        // if there isn´t any last saved token, we automatically know 
        // we don´t have any token and need to request a new one
        if ($checkForToken == null) {
            // request the new token from spotify
            $requestNewToken = $tokenNeeded->requestNewToken();
            $getValidToken = $tokenNeeded->getLastSavedToken();
            // var_dump($getValidToken);
            $newToken = $getValidToken[0]['token'];
            // var_dump($newToken);
            // exit();
        } else {
            // we got a valid token 
            // and can still use it 
            $newToken = $checkForToken[0]['token'];
        }
        //var_dump($newToken);exit();
        header('Content-type: application/json');
        //return json_encode([$newToken]);
        $array = array('token' => $newToken);
        //var_dump($array); exit();
        echo json_encode($array);
    }
}
