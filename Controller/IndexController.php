<?php
namespace Controller;

// use LogChecker;
use \Template;
use \App\SystemHelper as SH;

// class IndexController extends Controller {
class IndexController extends \App\LogChecker {
// class IndexController {
    public function index($f3, $params) {
        // https://fatfreeframework.com/3.6/framework-variables
        // Diese drei Variablen müssen immer gesetzt werden

        // var_dump($_SESSION);exit();
        $f3->set('content', '/views/content/home.php');

        // Template ausgeben
        echo Template::instance()->render('/views/new-layout.php');
        // echo Template::instance()->render('/views/new-starting-layout.php');
    }
}