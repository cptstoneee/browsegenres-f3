<?php

namespace Controller;

use \App\SystemHelper as SH;

class ItemController extends \App\LogChecker
{

    /**
     * Each time the modal (artist details) is opened - after clicking on one of the artist images in /browse or /browse/@genre - all data received by the ajax calls will be stored in db. The data inlcudes:
     * 1. artist details (name, id, popularity, ...), and
     * 2. artist searchterms, and
     * 3. 1 toptrack of the artist. toptrack detail includes data like name, id, popularity (of the track), preview_url,..., and
     * 4. artist images (size, url).
     * 
     * Each time before anything is stored in db, there´s a db query to check if it isn´t already there. 
     *
     * @return  [type]  [return description]
     */
    public function storeItemsArtist()
    {
        // get all necessary variables / parameters
        // see: https://developer.spotify.com/documentation/web-api/reference/artists/get-artist/
        $userId = $_SESSION['userId'][0]['user_id'];
        $artistResult = json_decode($_POST['json'], true);
        var_dump($artistResult);

        $artistName = $artistResult['result'][0]['name'];
        $artistExternalUrl = $artistResult['result'][0]['external_urls']['spotify'];
        $artistFollowers = $artistResult['result'][0]['followers']['total'];
        $artistHref = $artistResult['result'][0]['href'];
        $artistSpotifyId = $artistResult['result'][0]['id'];
        $artistPopularity = $artistResult['result'][0]['popularity'];
        $artistUri = $artistResult['result'][0]['uri'];
        $artistTime = time();
        $artistSearchterms = $artistResult['result'][0]['genres'];
        $myIdOfArtist = '';
        $artistImageUrl = '';
        $audioPreview = $artistResult['audioToptrack']['preview_url'];


        /****************************** 1. artist details | get first image of array *******************************/
        // $artistResultImage = $artistResult['result'][0]['images'];
        // var_dump($artistResultImage);
        $artistResultImage = $artistResult['result'][0]['images'][0];
        $string = '';
        foreach (array($artistResultImage) as $url => $string) {
            if ($url->$string !== null) {
                break;
            }
            $artistImageUrl = $string['url'];
            // var_dump($artistImageUrl);
        }

        /****************************** 1. artist details | store in table 'artists' *******************************/
        // check if the artist is already stored in db
        $im = new \Models\ItemModel();
        $getMyIdOfArtist = $im->getMyIdOfStoredArtist($artistSpotifyId);
        // returns the artist array
        // if artist isn´t in db query will return null/array is empty
        if (empty($getMyIdOfArtist)) {
            // if the array is empty -> artist can be stored
            $storeArtist = $im->storeItemsArtist(
                $artistName,
                $artistPopularity,
                $artistFollowers,
                $artistHref,
                $artistExternalUrl,
                $artistUri,
                $artistSpotifyId,
                $artistImageUrl,
                $audioPreview,
                $artistTime
            );
            $myIdOfArtist = $im->lastInsertedArtistMyId();
        } else {
            // otherwise return the id of the artist; by pointing to right value of array and convert it to integer 
            // "my id of [my] artist" | the id given by table (auto increment) | NOT id given by spotify ($artistSpotifyId)
            $myIdOfArtist = (int) $getMyIdOfArtist[0]['id'];
        }








        /****************************** 1. artist details |  store in lookup table 'user_artists' *******************************/
        // check if artist is in user_artists lookup table
        $artistIsStoredInLookupTable = $im->checkIfArtistIsInLookupTable($myIdOfArtist);
        // if array $artistIsStoredInLookupTable is empty, artist with myIdOfArtist can be stored; where userId
        if (empty($artistIsStoredInLookupTable)) {
            $storeArtistClickedByUser = $im->storeArtistsClickedByUser($userId, $myIdOfArtist, $artistTime);
        }

        $sm = new \Models\SearchtermModel();
        $artistSearchtermIsStoredInLookupTable = $sm->getArtistSearchterm($myIdOfArtist);
        $arr = [];
        foreach ($artistSearchtermIsStoredInLookupTable as $key => $val) {
            $arr[] = $val['searchterm_id'];
        }

        /****************************** 2. artist searchterms | check whether searchterm already exists in db or is new *******************************/
        // declare to empty arrays: 1 for all searchterms already stored (existing) in db; 1 for all new (non-existing) searchterms 
        $existingSearchterms = [];
        $notExistingSearchterms = [];
        foreach ($artistSearchterms as $currentSearchterm) {
            $sm = new \Models\SearchtermModel();
            // check table 'searchterms' if the current searchterm is stored in db; returns array
            $getIdOfSearchterm = $sm->getSearchterm($currentSearchterm);
            // if the array is empty, searchterm is not stored in db = new searchterm -> push to array $notExistingSearchterms
            if (empty($getIdOfSearchterm)) {
                $notExistingSearchterms[] = $currentSearchterm;
            } else {
                // already stored searchterm (existing) will be returned -> id pushed to array $
                //var_dump($arr); var_dump($currentSearchterm); exit();
                if (!in_array($getIdOfSearchterm[0]['searchterm_id'], $arr)) {
                    $existingSearchterms[] = $getIdOfSearchterm[0]['searchterm_id'];
                }
            }
        }
        /****************************** 2. artist searchterms | store in table 'searchterms' | return ids of these searchterms *******************************/
        foreach ($notExistingSearchterms as $newTerm) {
            $sm = new \Models\SearchtermModel();
            $sm->storeNotExistingSearchterm($newTerm);
            // returns the id of the currently stored searchterm
            $a = $sm->lastInsertedSearchtermId();
            // push currently stored searchterm id in array $existingSearchterms
            $existingSearchterms[] = $a;
        }
        /****************************** 2. artist searchterms | store in lookup table 'artist_searchterms' *******************************/
        // var_dump($myIdOfArtist);
        // var_dump($existingSearchterms);
        foreach ($existingSearchterms as $searchtermId) {
            $sm = new \Models\SearchtermModel();
            $sm->storeArtistSearchterms($myIdOfArtist, $searchtermId);
        }




        // pass "id of [my] artist"
        $this->storeItemsPreview($myIdOfArtist);
    }

    /**
     * [storeItemsPreview description]
     *
     * @param   [int]  $myIdOfArtist  [= "id of [my] artist" | id given by table 'artists' (auto incremented) | NOT id given by spotify]
     *
     * @return  [type]                 [return description]
     */
    public function storeItemsPreview($myIdOfArtist)
    {
        // get all variables/parameters needed
        // see: https://developer.spotify.com/documentation/web-api/reference/artists/get-artists-top-tracks/
        $userId = $_SESSION['userId'][0]['user_id'];
        $artistToptrack = json_decode($_POST['json'], true);

        $artistToptrackId = $artistToptrack['audioToptrack']['id']; // spotify id of the track
        $artistToptrackSpotifyArtistId = $artistToptrack['audioToptrack']['artists'][0]['id']; // spotify id of the artist
        $artistToptrackSpotifyArtistName = $artistToptrack['result'][0]['name']; // name of the artist

        // Known external URLs for this track. Opens Spotify album and highlights track.
        $artistToptrackSpotifyExternalUrlSpotify = $artistToptrack['audioToptrack']['external_urls']['spotify']; // external_url of the track

        // A link to the Web API endpoint providing full details of the track.
        $artistToptrackHref = $artistToptrack['audioToptrack']['href'];

        // Part of the response when Track Relinking is applied. If true , the track is playable in the given market ['given market' = ]. Otherwise false.
        $artistToptrackIsPlayable = $artistToptrack['audioToptrack']['is_playable'];

        $artistToptrackName = $artistToptrack['audioToptrack']['name']; // name of the track

        // The popularity of a track is a value between 0 and 100, with 100 being the most popular. 
        $artistToptrackPopularity = $artistToptrack['audioToptrack']['popularity'];

        // A link to a 30 second preview (MP3 format) of the track. Can be null
        $artistToptrackPreviewUrl = $artistToptrack['audioToptrack']['preview_url'];

        /* 
        The resource identifier that you can enter, for example, in the Spotify Desktop client’s search box to locate an artist, album, or track. To find a Spotify URI simply right-click (on Windows) or Ctrl-Click (on a Mac) on the artist’s or album’s or track’s name.
        See: https://developer.spotify.com/documentation/web-api/#spotify-uris-and-ids
        */
        $artistToptrackUri = $artistToptrack['audioToptrack']['uri'];

        /****************************** 3. artist toptrack | store in table 'toptracks' *******************************/
        // check (by using spotify track id) if track is already stored in db
        $im = new \Models\ItemModel();
        $idOfStoredToptrack = $im->getArtistToptrack($artistToptrackId);
        // if array $idOfStoredToptrack is empty -> store track in db
        if (empty($idOfStoredToptrack)) {
            $storeToptrackInToptracks = $im->storeItemsPreview(
                (int) $myIdOfArtist,
                $artistToptrackSpotifyArtistName,
                $artistToptrackSpotifyArtistId,
                $artistToptrackName,
                $artistToptrackId,
                $artistToptrackSpotifyExternalUrlSpotify,
                $artistToptrackPopularity,
                $artistToptrackPreviewUrl,
                $artistToptrackHref,
                $artistToptrackUri,
                $artistToptrackIsPlayable
            );
            // get id of the currently stored track in db
            $myIdOfToptrack = $im->lastInsertedArtistToptrackId();

            // $storeToptrackInArtists = $im->store
        } else {
            // otherwise if array is not empty = track is already stored -> return id
            $im = new \Models\ItemModel();
            $idOfStoredToptrack = $im->getArtistToptrack($artistToptrackId);
            $myIdOfToptrack = $idOfStoredToptrack;
        }
    }



    public function deleteItemsArtist($f3, $params)
    {
        $artistId = $params['aid'];
        // var_dump($params);
        // var_dump($artistId);
        // exit();
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($artistId, FILTER_VALIDATE_INT)) {
            echo 'Error: User konnte nicht gelöscht werden';
        } else {
            $im = new \Models\ItemModel();
            if ($im->deleteArtist($artistId)) {
                $f3->reroute('/dashboard');
                // echo '1'; // Bei Erfolg wird 1 zurück gegeben
            } else {
                echo 'Error';
            }
        }
    }
}
