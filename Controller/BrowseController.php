<?php

namespace Controller;

use \Template;
use App\SystemHelper as SH;

// use \Controller\SpotifySearches;

// class BrowseController extends Controller
class BrowseController extends \App\LogChecker
// class BrowseController
{

    // public function __construct()
    // {
    //     // we need a valid token for the genre query
    //     $tokenNeeded = new \SpotifyTokenManager();
    //     // check db for the last saved entry
    //     // (by asking if the current timestamp is < than the expiration time of the saved token)
    //     // if this valuets to true the db returns the valid token
    //     // otherwise we know that we have to request a new token
    //     $checkForToken = $tokenNeeded->getLastSavedToken();
    //     // if there isn´t any last saved token, we automatically know 
    //     // we don´t have any token and need to request a new one
    //     if ($checkForToken == null) {
    //         // request the new token from spotify
    //         $requestNewToken = $tokenNeeded->requestNewToken();
    //         $newToken = $requestNewToken[1][0]['token'];
    //     } else {
    //         // we got a valid token 
    //         // and can still use it 
    //         $newToken = $checkForToken[0]['token'];
    //     }
    // }

    public function index($f3, $params)
    {

        // if (!SH::customSessionRead('userId')) {
        // if (!$_SESSION['userId'] || !$_SESSION['userId'][0]['user_id']) {
        //     // if (!$this->customSessionRead('userId')) {
        //     // var_dump($_SESSION['userId']);

        // }

        if (isset($_SESSION['userId'][0]['user_id'])) {
            $userIdLang = $_SESSION['userId'][0]['user_id'];
            var_dump($userIdLang);
        } elseif (isset($_SESSION['userId'])) {
            $userIdKurz = $_SESSION['userId'];
            var_dump($userIdKurz);
        } else {
            $f3->reroute('/login');
        }



        $bm = new \Models\BrowseModel();
        $genres = $bm->genres();
        $f3->set('genres', $genres);

        $f3->set('content', '/views/content/browse.php');

        echo Template::instance()->render('/views/new-layout.php');
    }


    /**
     * Gets genre term out of url and calls Spotify item search search
     *
     * @param Object $f3
     * @param array $params
     * @return void
     */
    public function show($f3, $params)
    {

        // if (isset($_SESSION['userId'][0]['user_id'])) {
        //     $userId = $_SESSION['userId'][0]['user_id'];
        // } else {
        //     $userId = $_SESSION['userId'];
        // }

        $userId = SH::customSessionRead('userId', $_SESSION);

        // if (isset($_SESSION['userId'])) {
        //     $userId = $_SESSION['userId'];
        // } else {
        //     $userId = $_SESSION['userId'][0]['user_id'];
        // }

        // var_dump($userId);
        // step 1 | get search term out of url
        $genreName = $params['genre']; // get desired genre name out of URL parameter (/browse/@genre)
        // var_dump($genreName);
        // exit();
        
        $um = new \Models\BrowseModel(); //instantiate new BrowseModel object and save values in $um
        $genre = $um->genre($genreName); // call BrowseModel method 'genre' and return value in $genre
        // var_dump($_SESSION);
        //  $f3->set('token', $_SESSION['token']);
        
        // step 2 | call Spotify Method
        $newSearch = new \App\SpotifySearches(); // instaniate new object of SpotifySearches class
        // $newSearch = new \SpotifySearchesCURL(); // instaniate new object of SpotifySearches class
        
        $f3->set('genreName', $genreName); // pass current search term to $f3
        
        // var_dump($params);
        // exit();
        $newResults = $newSearch->genre($genre, $params); // call SpotifySearches method and store result in $newResults


        foreach ($newResults->artists->items as $artist) {
            $f3->set('results', $newResults);
        }

        $f3->set('content', '/views/content/browse-show.php');
        $f3->set('jScripts', ['/js/modal_combineGenres.js']);

        echo Template::instance()->render('/views/new-layout.php');
    }
}
