<?php
namespace Controller;

use \Template;
use \App\SystemHelper as SH;

class ContactController extends \App\LogChecker {
// class ContactController {
    public function index($f3, $params) {
        // https://fatfreeframework.com/3.6/framework-variables
        // Diese drei Variablen müssen immer gesetzt werden

        $f3->set('content', '/views/content/contact.php');

        // Template ausgeben
        echo Template::instance()->render('/views/new-layout.php');
    }
}