<?php

namespace Controller;

use \Template;
use \App\SystemHelper as SH;


// class MainController extends Controller {
class MainController extends \App\LogChecker
{
    /**
     * [show description]
     *
     * @param   [type]  $f3      [$f3 description]
     * @param   [type]  $params  [$params description]
     *
     * @return  [type]           [return description]
     */
    public function show($f3, $params)
    {
        //get userId
        if (isset($_SESSION['userId'][0]['user_id'])) {
            $userId = $_SESSION['userId'][0]['user_id'];
            // var_dump($userId);
        } else {
            $userId = $_SESSION['userId'];
            // var_dump($userId);
        }

        // $userId = SH::customSessionRead('userId');
        // var_dump($userId);

        $im = new \Models\ItemModel();
        
        /************** get the amount of artists a user has ***************/
        // get all artists clicked by the user
        $getArtists = $im->getArtistsClickedByUser($userId);
        // nested array; to get the size of the array, it´s necessary to parse through it and write key-values-pairs
        $arrArtist = [];
        foreach ($getArtists as $key => $val) {
            $arrArtist[] = $val;
        }
        // now the key/value pairs can be counted
        $countArtists = count($arrArtist);
        $f3->set('amountArtists', $countArtists);
        

        /************** get the amount of searchterms a user has ***************/
        // get all searchterms, but comes with duplicates
        $getSearchterms = $im->getSearchtermsClickedByUser($userId);

        // get rid of the duplicates
        function array_unique_multidimensional($getSearchterms)
        {
            $serialized = array_map('serialize', $getSearchterms);
            $unique = array_unique($serialized);
            return (array_intersect_key($getSearchterms, $unique));
        }
        
        // call function and store all cleared key/value pairs in $searchtermDuplicatesRemoved
        $searchtermDuplicatesRemoved = array_unique_multidimensional($getSearchterms);
      
        $arr = [];
        foreach ($searchtermDuplicatesRemoved as $key => $val) {
            $arr[] = $val;
        }
        // now the key/value pairs can be counted
        $countArray = count($arr);

        // if a user hasn´t stored yet any artist/searchterm, render empty dashboard
        if (empty($getArtists) || empty($getSearchterms)) {
            $f3->set('content', '/views/content/dashboard-empty.php');
        } else {
            foreach ($getArtists as $artist) {
                $f3->set('results', $getArtists);
                // var_dump($getArtists);
                // exit();
            }
            $f3->set('jScripts', ['/js/delete-artist-dashboard.js']);
            $f3->set('content', '/views/content/userDashboard.php');
        }
        // exit();



        // Template ausgeben
        echo Template::instance()->render('/views/new-layout.php');
    }
}
