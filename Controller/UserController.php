<?php

namespace Controller;

use \Template;
use \App\SystemHelper as SH;

// class UserController extends Controller
// class UserController extends SystemHelper
class UserController extends \App\LogChecker
{
    // class UserController


    public function register($f3, $params)
    {

        // form has been posted
        if (!empty($_POST)) {

            $gump = new \GUMP('en');

            // gump validates form entries upon these rules
            $gump->validation_rules(array(
                'username' => 'required|alpha_numeric|max_len,100|min_len,6',
                'password' => 'required|alpha_numeric|max_len,255|min_len,8',
                'email'    => 'required|valid_email|max_len,255'
            ));

            // further form entry sanitation
            $gump->filter_rules(array(
                'username' => 'trim|sanitize_string',
                'password' => 'trim',
                'email'    => 'trim|sanitize_email'
            ));

            // initialize gump
            $validData = $gump->run($_POST);

            // if form entries don´t pass the gump rules, fire error messages
            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            } else {
                // validated form entries need to be checked if there are already username and/or email stored in database 
                $um = new \Models\UserModel();
                $alreadyTakenUsername = $um->getUserName($validData['username']);
                $alreadyTakenEmail = $um->getUserEmail($validData['email']);
                // form is valid
                if (($alreadyTakenUsername[0]['user_id'] > 0) || (($alreadyTakenEmail[0]['user_id']) > 0)) {
                    $f3->set('alertError', 'Error! Username or E-Mail already taken!');
                    // data is sanitized
                    // validated and unique will be stored in db now
                } else {
                    // generate verification key (vkey)
                    $username = $_POST['username'];
                    $email = $validData['email'];
                    $vkey = md5(time().$username);
                    // store data into the database
                    $hashedPassword = md5($validData['password']);
                    
                    $isStored = $um->storeUser($validData['username'], $hashedPassword, $validData['email'], $vkey);
                    // retrieves user_id of just stored user credentials
                    $last_id = $um->lastInsertedId();
                } // once stored in db, user will be redirected to his dashboard
                if ($isStored === true) {
                    SH::customSessionStore('userId', $last_id);
                    // send verification email to user
                    $to = $email;
                    $subject = "Email verification";
                    $message = "<a href='http://localhost/registration/verify.php?vkey=$vkey'>Register Account</a>";
                    $headers = "From: browsegenres@gmail.com";
                    $headers .= "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                    mail($to, $subject, $message, $headers);



                    $f3->reroute('/dashboard');
                    // $f3->reroute('/browse');
                } else {
                    $f3->set('alertError', 'A file with the same name and/or email already exists. Please specify another name and/or email.');
                }
            }
        }

        $f3->set('content', '/views/content/register.php');

        echo Template::instance()->render('/views/new-layout-register.php');
    }




    public function registrationemailconfirmed($f3, $params)
    {

        $f3->set('content', '/views/content/thankyou.php');

        // Template ausgeben
        echo Template::instance()->render('/views/new-layout-register.php');
    }



    public function login($f3, $params)
    {

        $f3->set('content', '/views/content/login.php');

        // Template ausgeben
        echo Template::instance()->render('/views/new-layout-register.php');
    }




    function authenticate($f3, $params)
    {

        if (isset($_POST['username']) and isset($_POST['password'])) {

            $gump = new \GUMP('en');

            // gump validates form entries upon these rules
            $gump->validation_rules(array(
                'username' => 'required|alpha_numeric|max_len,100|min_len,6',
                'password' => 'required|alpha_numeric|max_len,255|min_len,8',

            ));

            // further form entry sanitation
            $gump->filter_rules(array(
                'username' => 'trim|sanitize_string',
                'password' => 'trim',
            ));

            // initialize gump
            $validLoginData = $gump->run($_POST);

            // getting username and password from sent POST
            $username = $validLoginData['username'];
            $password = $validLoginData['password'];

            // Connect to server and select databse.
            $user = new \Models\UserModel($this->db);
            $result = $user->getUserCredentials($username, $password);
            if ($result !== false) {
                // start session and redirect to user dashboard
                // $this->customSessionStore('userId', $result);
                SH::customSessionStore('userId', $result);
                // var_dump(SH::customSessionRead('userId'));
                // exit();
                $f3->reroute('/dashboard');
            } else {
                // redirect login
                // error
                $f3->set('values', $_POST);
                $f3->set('loginError', 'You have entered an invalid username or password');
                // $f3->reroute('/login');
                $f3->set('content', '/views/content/login.php');

                echo Template::instance()->render('/views/new-layout-register.php');
            }
        }
    }



    public function logout($f3, $params)
    {
        // $this->customSessionDestroy();
        SH::customSessionDestroy();
        $f3->reroute('/');
    }
}
