<?php
namespace Models;

class BrowseModel extends Model {
    /**
     * Find out all genres
     *
     * @return array
     */
    public function genres() : array {
        return $this->db->exec('SELECT * FROM genres');
    }

    /**
     * Find out single genre
     *
     * @param string $genreName
     * @return array
     */
    public function genre(string $genreName) : array {
        $genre = $this->db->exec('SELECT * FROM genres WHERE genre_name=?', $genreName);

        return $genre;
    }

}