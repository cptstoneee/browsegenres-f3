<?php

namespace Models;

abstract class Model
{
    protected $db;

    // TODO: move access credentials to init.php
    public function __construct()
    {
        // db Save connection at class attribute
        $this->db = new \DB\SQL(
            'mysql:host=localhost;port=3306;dbname=browsegenres',
            'root',
            ''
        );
    }
}
