<?php

namespace Models;

class SearchtermModel extends Model
{
    public function storeSearchterm(string $currentSearchterm, int $total, int $searchtermTimestamp): bool
    {
        return $this->db->exec('INSERT INTO searchterms (searchterm_name, results_given, timestamp) VALUES(?, ?, ?)', [$currentSearchterm, $total, $searchtermTimestamp]);
    }


    public function storeNotExistingSearchterm(string $newTerm)
    {
        $timestamp = time();
        return $this->db->exec('INSERT INTO searchterms (searchterm_name, timestamp) VALUES(?, ?)', [$newTerm, $timestamp]);
    }



    public function lastInsertedSearchtermId()
    {
        return $this->db->pdo()->lastInsertId();
    }



    public function getSearchterm(string $currentSearchterm) : array
    {
        $searchterm = $this->db->exec('SELECT * FROM searchterms WHERE searchterm_name=?', $currentSearchterm);

        return $searchterm;
    }



    public function storeArtistSearchterms($myIdOfArtist, $searchtermId)
    {
        return $this->db->exec('INSERT INTO artist_searchterms (artist_id, searchterm_id) VALUE(?, ?)', [$myIdOfArtist, $searchtermId]);
    }


    public function getArtistSearchterm($myIdOfArtist)
    {
        $artistSearchterm = $this->db->exec('SELECT searchterm_id FROM artist_searchterms WHERE artist_id=?', $myIdOfArtist);

        return $artistSearchterm;
    }













    public function storeUserSearchterm(int $userId, int $searchTermId, int $total, int $searchtermTimestamp)
    {
        return $this->db->exec('INSERT INTO user_searchterms (user_id, searchterm_id, results_given, timestamp) VALUES(?, ?, ?, ?)', [$userId, $searchTermId, $total, $searchtermTimestamp]);
    }
}
