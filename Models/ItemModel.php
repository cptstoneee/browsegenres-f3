<?php

namespace Models;

class ItemModel extends Model
{
    // store artist 
    public function storeItemsArtist(
        string $artistName,
        int $artistPopularity,
        int $artistFollowers,
        string $artistHref,
        string $artistExternalUrl,
        string $artistUri,
        string $artistSpotifyId,
        string $artistImageUrl,
        string $audiopreview,
        int $artistTime)
    {
        return $this->db->exec('INSERT INTO artists (
            name,
            spotify_popularity,
            spotify_followers,
            spotify_artist_href,
            spotify_artist_external_url,
            spotify_artist_uri,
            spotify_id,
            artist_image,
            artist_toptrack_preview,
            timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                $artistName,
                $artistPopularity,
                $artistFollowers,
                $artistHref,
                $artistExternalUrl,
                $artistUri,
                $artistSpotifyId,
                $artistImageUrl,
                $audiopreview,
                $artistTime]);
    }

    public function lastInsertedArtistMyId()
    {
        return $this->db->pdo()->lastInsertId();
    }


    // get my id of artist IF already stored
    public function getMyIdOfStoredArtist(string $artistSpotifyId): array
    {
        $artistMyId = $this->db->exec('SELECT * FROM artists WHERE spotify_id=?', [$artistSpotifyId]);

        return $artistMyId;
    }


    public function checkIfArtistIsInLookupTable(int $myIdOfArtist)
    {
        $artistIsstored = $this->db->exec('SELECT artist_id FROM user_artists WHERE artist_id=?', [$myIdOfArtist]);
        // if ($artistIsstored != '') {
        //     return true;
        // } else {
        //     return false;
        // }
        return $artistIsstored;
    }


    public function storeArtistsClickedByUser(int $userId, int $myIdOfArtist, int $artistTime)
    {
        return $this->db->exec('INSERT INTO user_artists (user_id, artist_id, timestamp) VALUES(?, ?, ?)', [$userId, $myIdOfArtist, $artistTime]);
    }


    public function getArtistsClickedByUser(int $userId)
    {
        return $this->db->exec('SELECT id, name, spotify_popularity, spotify_followers, spotify_artist_external_url, artist_image, artist_toptrack_preview
                                FROM artists a
                                INNER JOIN user_artists ua
                                ON ua.artist_id = a.id
                                WHERE ua.user_id=?', [$userId]);
    }


    public function getSearchtermsClickedByUser(int $userId)
    {
        return $this->db->exec('SELECT searchterms.searchterm_id, searchterms.searchterm_name 
                                FROM searchterms 
                                INNER JOIN user_searchterms 
                                ON user_searchterms.searchterm_id = searchterms.searchterm_id
                                WHERE user_searchterms.user_id=?', [$userId]);
    }


    // store artist 
    public function storeItemsPreview(
        int $myIdOfArtist,
        string $artistToptrackSpotifyArtistName,
        string $artistToptrackSpotifyArtistId,
        string $artistToptrackName,
        string $artistToptrackId,
        string $artistToptrackSpotifyExternalUrlSpotify,
        int $artistToptrackPopularity,
        string $artistToptrackPreviewUrl,
        string $artistToptrackHref,
        string $artistToptrackUri,
        bool $artistToptrackIsPlayable
    ) {
        $timestamp = time();
        return $this->db->exec('INSERT INTO toptracks (
            artist_id,
            spotify_artist_name,
            spotify_artist_id,
            spotify_track_name,
            spotify_track_id,
            spotify_track_externalurl_spotify,
            spotify_track_popularity,
            spotify_track_preview_url,
            spotify_track_href,
            spotify_track_uri,
            spotify_is_playable,
            timestamp ) VALUES(?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
            $myIdOfArtist,
            $artistToptrackSpotifyArtistName,
            $artistToptrackSpotifyArtistId,
            $artistToptrackName,
            $artistToptrackId,
            $artistToptrackSpotifyExternalUrlSpotify,
            $artistToptrackPopularity,
            $artistToptrackPreviewUrl,
            $artistToptrackHref,
            $artistToptrackUri,
            $artistToptrackIsPlayable,
            $timestamp
        ]);
    }


    public function lastInsertedArtistToptrackId()
    {
        return $this->db->pdo()->lastInsertId();
    }



    public function getArtistToptrack(string $artistToptrackId): array
    {
        $artistToptrackIsstored = $this->db->exec('SELECT * FROM toptracks WHERE spotify_track_id=?', [$artistToptrackId]);
        return $artistToptrackIsstored;
    }

    public function deleteArtist(int $artistId) {
        $isDeleted = $this->db->exec('DELETE FROM artists WHERE id=?', $artistId);
        return $isDeleted;
    }

}
