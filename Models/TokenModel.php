<?php

namespace Models;

class TokenModel extends Model
{

    public function checkTokenExistence()
    {
        $tokenExists = $this->db->exec('SELECT COUNT(1) FROM spotify_access_token WHERE spotify_access_token_id = 1');
        return $tokenExists;
    }

    public function getExpirationTimeFromSavedToken(int $expirationTimefromSavedToken): int
    {
        $expirationTimefromSavedToken = $this->db->exec('SELECT expirationTime FROM spotify_access_token');

        // if (count($user) === 0) {
        //     return [];
        // }

        // return $user[0];
        return $expirationTimefromSavedToken;
    }

    public function storeToken(string $accesstoken, int $tokenExpirationTime): bool
    {
        return $this->db->exec('INSERT INTO spotify_access_token (token, expirationTime) VALUES(?, ?)', [$accesstoken, $tokenExpirationTime]);
    }

    public function updateToken(string $accesstoken, int $tokenExpirationTime)
    {
        $isUpdated = $this->db->exec('UPDATE spotify_access_token SET (token, expirationTime) VALUES(?, ?)', [$accesstoken, $tokenExpirationTime]);
    }


    public function getToken(int $currentTime)
    {
        return $this->db->exec('SELECT token FROM spotify_access_token WHERE expirationTime >' . $currentTime);
    }

    public function lastInsertedTokenId()
    {
        return $this->db->pdo()->lastInsertId();
    }
}
