<?php
// main page / index
$f3->route('GET /', 'Controller\IndexController->index');

// spotify related
$f3->route('GET /checktoken', 'Controller\TokenController->gettoken');
$f3->route('GET /browse', 'Controller\BrowseController->index');
$f3->route('GET /browse/@genre', 'Controller\BrowseController->show');

// user management
$f3->route('POST /register', 'Controller\UserController->register');
$f3->route('GET /register', 'Controller\UserController->register');
$f3->route('GET /login', 'Controller\UserController->login');
$f3->route('GET /thankyou', 'Controller\UserController->registrationemailconfirmed');
$f3->route('POST /authenticate', 'Controller\UserController->authenticate');
$f3->route('GET /dashboard', 'Controller\MainController->show');
$f3->route('GET /logout', 'Controller\UserController->logout');

// cart management
$f3->route('POST /addtocartartist', 'Controller\ItemController->storeItemsArtist');
$f3->route('GET /addtocartartist', 'Controller\ItemController->storeItemsArtist');
$f3->route('POST /addtocartpreview', 'Controller\ItemController->storeItemsPreview');
$f3->route('GET /deletefromcart/@aid/artist', 'Controller\ItemController->deleteItemsArtist');

// additional content pages
$f3->route('GET /about', 'Controller\AboutController->index');
$f3->route('GET /newsletter', 'Controller\NewsletterController->index');
$f3->route('GET /contact', 'Controller\ContactController->index');
$f3->route('GET /privacy', 'Controller\PrivacyController->index');
$f3->route('GET /terms', 'Controller\TermsController->index');









