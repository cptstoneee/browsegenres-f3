(function() {
  var artistId = "";

  $(".artist").click(function() {
    var artistId = $(this).attr("data-id");
    var token = $(this).attr("data-token");

    // requests details of the artist for the modal view
    var getArtist = $.ajax({
      url: "https://api.spotify.com/v1/artists/" + artistId,
      headers: { Authorization: "Bearer " + token },
      success: function(result) {
        // console.log("getartist");
        console.log(result);
        // console.log(result[0].genres);
      }
    });

    // requests the top tracks of the artists, needed for audio preview
    var getTopTrack = $.ajax({
      // todo: get country code from session or user ip
      url:
        "https://api.spotify.com/v1/artists/" +
        artistId +
        "/top-tracks?country=US", // country needs to be set but should be taken from user´s ip address
      headers: { Authorization: "Bearer " + token },
      success: function(toptrack) {
        console.log(toptrack);
      }
    });

    // build html for modal content
    $.when(getArtist, getTopTrack).done(function(result, toptrack) {
      console.log(toptrack[0].tracks);
      var artistImg =  '<img class=artistImg src="' + result[0].images[2].url + '" height=160 width=160/>'
      var artistName = '<h2>' + result[0].name + '</h2>';
      var popularityNr = result[0].popularity;
      var popularity = '<h3>Popularity</h3>'
      var followersNr = numeral(result[0].followers.total).format('0,0');
      var followers = '<h2>Followers</h2>'

      var genreNames = '';
      for (var i = 0; i < result[0].genres.length; i++) {
        genreNames += '<div class="d-flex"><button type="button" class="btn btn-secondary">' + result[0].genres[i] + '</button><button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent"><span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" aria-labelledby="dropdownMenuReference"><a class="dropdown-item" id="whatIsIt_data-id:"' + result[0].name + '" href="#">What is it?</a><a class="dropdown-item" id="startNewSearch" href="#">Start new search</a><a class="dropdown-item" id="combine" href="#">Combine with another</a><div class="dropdown-divider"></div><a class="dropdown-item" id="favorite" href="#">Add to favorites</a></div></div></div>'
        // genreNames += '<div class="d-flex"><button type="button" class="btn btn-secondary">' + result[0].genres[i] + '</button><button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent"><span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu" aria-labelledby="dropdownMenuReference"><a class="dropdown-item" [attribute="'+result[0].genres[i]+'"] href="#">What is it?</a><a class="dropdown-item" id="startNewSearch" href="#">Start new search</a><a class="dropdown-item" id="combine" href="#">Combine with another</a><div class="dropdown-divider"></div><a class="dropdown-item" id="favorite" href="#">Add to favorites</a></div></div></div>'
      };



      //**************************************************************** */
      /* 
      Some artists don´t have audio previews at all or don´t provide for the country in question.
      So we just want to show IF key (preview_url) has a value (audio source) and is not null.

      Array.filter:
      Return value: new array consisting only of items that passed a condition.
      https://www.freecodecamp.org/news/the-complete-guide-to-loops-in-javascript-f5e242921d8c/
      */
      var numbers = [toptrack[0].tracks[i].preview_url];
      var condition = numbers => numbers != null;
      var filtered = numbers.filter(condition);
      console.log(filtered);

      if (filtered > 0) {
        var song =
          '<audio controls><source src="' +
          filtered +
          '" type="audio/ogg" + class="toptrack"> <source src=""' +
          filtered +
          '" type="audio/mpeg" + class="toptrack"></audio>';
      } else var song = "";


      console.log(filtered);
      //**************************************************************** */

      // call / show modal
      $("#getArtistImg").html(artistImg);
      $("#getPopularityNr").html(popularityNr);
      $("#getPopularity").html(popularity);
      $("#getFollowersNr").html(followersNr);
      $("#getFollowers").html(followers);
      $("#getArtistName").html(artistName);
      $("#getGenreNames").html(genreNames);
      $("#getSong").html(song);
      $(".modal").modal("show");
      console.log(result);
      console.log(toptrack);

    });
  });
})();
