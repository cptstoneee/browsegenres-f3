(function() {
  var artistId = "";
  var token = "";

  $(".artist").click(function() {
    var artistId = $(this).attr("data-id");
    // var token = $(this).attr("data-token");

    // requests details of the artist for the modal view
    var getArtist = $.ajax({
      url: "/checktoken",
      success: function(t) {
        token = t.token;
        $.ajax({
          url: "https://api.spotify.com/v1/artists/" + artistId,
          headers: { Authorization: "Bearer " + token },
          success: function(result) {
            // console.log("getartist");
            // console.log(result);
            // console.log(result[0].genres);
          }
        })
      }
    });

    // requests the top tracks of the artists, needed for audio preview
    var getTopTrack = $.ajax({
      // todo: get country code from session or user ip
      url:
        "https://api.spotify.com/v1/artists/" +
        artistId +
        "/top-tracks?country=US", // country needs to be set but should be taken from user´s ip address
      headers: { Authorization: "Bearer " + token },
      success: function(toptrack) {
        // console.log(toptrack);
      }
    });

    // build html for modal content
    $.when(getArtist, getTopTrack).done(function(result, toptrack) {
      // console.log(toptrack[0].tracks);
      var followers = result[0].followers.total;
      var artistImg =
        '<img class="artist--img" id="artist--img" src="' +
        result[0].images[2].url +
        '"/>'; // todo: find rule for img (some only have 64 px, others are bigger; maybe fix frame?)
      var artistName = "<p>" + result[0].name + "</p>";
      var popularityNr =
        '<p class="popularity-nr" id="popularity-nr">' +
        result[0].popularity +
        "</p>";
      var popularity =
        '<p class="popularity-label" id="popularity-label" >Popularity</p>';
      var followersNr =
        '<p class="followers-nr" id="followers-nr">' +
        numeral(result[0].followers.total).format("0,0") +
        "</p>";
      var followers =
        '<p class="followers-label" id="followers-label" >Followers</p>';

      // this finds and builds the genre names / tags
      // includes onclick function for plus img & search img (see below)
      var genreNames = "";
      for (var i = 0; i < result[0].genres.length; i++) {
        genreNames +=
          // '<div class="genre-names"><p>'+result[0].genres[i] +
          // '</p><img src="/css/icons/plus.svg" onclick="combineGenres(\'' +
          // result[0].genres[i] +
          // '\')" class="imgPlus" id="img-plus" alt="add" width="42" height="42">';
          '<div class="genre-names"><p>'+result[0].genres[i] +
          '</p><img src="/css/icons/plus.svg" onclick="combineGenres(\'' +
          result[0].genres[i] +
          '\')" class="imgPlus" id="img-plus" alt="add" width="42" height="42"></div>';
      }

      /* 
      Some artists don´t have audio previews at all or don´t provide for the country in question.
      So we just want to show IF key (preview_url) has a value (audio source) and is not null.

      Array.filter:
      Return value: new array consisting only of items that passed a condition.
      https://www.freecodecamp.org/news/the-complete-guide-to-loops-in-javascript-f5e242921d8c/
      */
      var numbers = [toptrack[0].tracks[i].preview_url];
      // console.log(numbers);
      var condition = numbers => numbers != null;
      var filtered = numbers.filter(condition);
      // console.log(filtered);

      var song =
        '<audio controls id="audio-track"><source src="' +
        filtered +
        '" type="audio/ogg"><source src="' +
        filtered +
        '" type="audio/mpeg"></audio>';

      // console.log(filtered);

      $(".artistImg").html(artistImg);
      $(".popularityNr").html(popularityNr);
      $(".popularityLabel").html(popularity);
      $(".followersNr").html(followersNr);
      $(".followersLabel").html(followers);
      $(".artistName").html(artistName);
      $(".genreNames").html(genreNames);
      $(".song").html(song);
      $(".modal").modal("show");
      // console.log(result);
      // console.log(toptrack);
    });
  });
})();

var genresSaved = [];
// var clickedGenreName = false;

function combineGenres(genres) {
  clickedGenreName = true;
  var clickedQueryString = genres;
  if (genresSaved.includes(clickedQueryString)) {
    newQuery = genresSaved;
    // console.log(newQuery);
  } else genresSaved.push(clickedQueryString);
  // console.log(genresSaved);

  newQuery = genresSaved.toString();
  // console.log(newQuery);
  var combinedNewQuery = newQuery.replace([','], " & ");
  // console.log(combinedNewQuery);

  /* 
  The \s meta character in JavaScript regular
  expressions matches any whitespace character:
  spaces, tabs, newlines and Unicode spaces.
  And the g flag tells JavaScript to replace it multiple times.
  If you miss it, it will only replace the first occurrence of the white space.
  */
  var erasedEtSign = combinedNewQuery.replace(['&'], '');
  // console.log(erasedEtSign);
  var doublePlusAndEtSign = erasedEtSign.replace(['+&+'], '+');
  // console.log(doublePlusAndEtSign);
  var doublePlus = doublePlusAndEtSign.replace(/\s/g, "+");
  var readyForSpotify = doublePlus.replace(['++'], '+');


  // var readySearchQuery = '"' + gluedSearchQuery + '"';
  // console.log(readyForSpotify);
  // console.log(readySearchQuery);
  var basicRoute = "http://browsegenres-f3.loc/browse/";
  var message = "<p>Get new result for: " + combinedNewQuery + "</p>"; // todo: delete this message when user closes modal but stays on same site (no site reload)
  var newQuery = basicRoute + readyForSpotify;
  // console.log(message, newQuery);
  var searchButton =
    "<a href=" +
    basicRoute +
    readyForSpotify +
    '><button class="btn btn-primary">OK!</button></a>';
  var modalFooterOutput = message + searchButton;
  $(".modal-footer").html(modalFooterOutput);
  $(".modal").modal("show"); 
}
