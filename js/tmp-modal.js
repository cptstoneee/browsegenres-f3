(function() {
  var artistId = "";

  $(".artist").click(function() {
    var artistId = $(this).attr("data-id");
    var token = $(this).attr("data-token");

    // requests details of the artist for the modal view
    var getArtist = $.ajax({
      url: "https://api.spotify.com/v1/artists/" + artistId,
      headers: { Authorization: "Bearer " + token },
      success: function(result) {
        // console.log("getartist");
        console.log(result);
        // console.log(result[0].genres);
      }
    });

    // requests the top tracks of the artists, needed for audio preview
    var getTopTrack = $.ajax({
      // todo: get country code from session or user ip
      url:
        "https://api.spotify.com/v1/artists/" +
        artistId +
        "/top-tracks?country=US", // country needs to be set but should be taken from user´s ip address
      headers: { Authorization: "Bearer " + token },
      success: function(toptrack) {
        console.log(toptrack);
      }
    });

    // build html for modal content
    $.when(getArtist, getTopTrack).done(function(result, toptrack) {
      console.log(toptrack[0].tracks);
      var followers = result[0].followers.total;
      var text =
        "<h2>" +
        result[0].name +
        "</h2>" +
        "<br>" +
        "<h3> Followers: " +
        numeral(followers).format("0,0") +
        "</h3>" +
        "<br>" +
        "<h3> Popularity: " +
        result[0].popularity +
        "</h3>" +
        "<br>";

      for (var i = 0; i < result[0].genres.length; i++) {
        text += '<div><h4>' + result[0].genres[i] + '</h4><img src="/css/icons/plus.svg" alt="add" width="42" height="42">' + '<img src="/css/icons/search.svg" alt="add" width="30" height="30"></div>'
      }

      /* 
      Some artists don´t have audio previews at all or don´t provide for the country in question.
      So we just want to show IF key (preview_url) has a value (audio source) and is not null.

      Array.filter:
      Return value: new array consisting only of items that passed a condition.
      https://www.freecodecamp.org/news/the-complete-guide-to-loops-in-javascript-f5e242921d8c/
      */
      var numbers = [toptrack[0].tracks[i].preview_url];
      var condition = numbers => numbers != null;
      var filtered = numbers.filter(condition);

      if (filtered > 0) {
        var song =
          '<audio controls><source src="' +
          filtered +
          '" type="audio/ogg" + class="toptrack"> <source src=""' +
          filtered +
          '" type="audio/mpeg" + class="toptrack"></audio>';
      } else var song = "";

      console.log(filtered);

      // call / show modal
      $("#getCodeText").html(text + "<br>" + song);
      $("#getCodeText").html(text + "<br>" + song);
      $(".modal").modal("show");
      console.log(result);
      console.log(toptrack);
    });
  });
})();
