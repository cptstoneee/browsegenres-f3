<?php

namespace App;

use \Template;
use \SpotifyWebAPI;
use \Session;
// use \App\SystemHelper as SH;
// use \SpotifyTokenManager;

class SpotifySearches extends Session
{

    public $searchGenre;
    public $session;
    public $accessToken;
    public $currentSearchTerm;
    public $timeout = '';
    public $newToken = '';


    public function genre($f3, $params)

    {
        // var_dump(SH::customSessionRead('user_id'));
        // var_dump($_SESSION['userId']);exit();

        $currentSearchTerm = preg_replace('/\s+/', '+', $params['genre']);


        // we need a valid token for the genre query
        $tokenNeeded = new \App\SpotifyTokenManager();
        // check db for the last saved entry
        // (by asking if the current timestamp is < than the expiration time of the saved token)
        // if this valuets to true the db returns the valid token
        // otherwise we know that we have to request a new token
        $checkForToken = $tokenNeeded->getLastSavedToken();
        // var_dump($checkForToken);
        // exit();

        // if there isn´t any last saved token, we automatically know 
        // we don´t have any token and need to request a new one
        if ($checkForToken == null) {
            // request the new token from spotify
            $requestNewToken = $tokenNeeded->requestNewToken();
            $getValidToken = $tokenNeeded->getLastSavedToken();
            // var_dump($getValidToken);
            $newToken = $getValidToken[0]['token'];
            // var_dump($newToken);
            // exit();
        } else {
            // we got a valid token 
            // and can still use it 
            $newToken = $checkForToken[0]['token'];
        }

        // activate the query | instantiate object
        $api = new SpotifyWebAPI\SpotifyWebAPI();
        $api->setAccessToken($newToken);

        // the current search we want the results for
        $query = 'genre:"' . $currentSearchTerm . '"';
        // var_dump($query);exit();

        $type = 'artist';
        $options = [
            'market' => 'US',
            'limit' => '50',
            'offset' => '0'
        ];

        /* 
        add current valid token to the restults object
        this is imports for the output in browse-show.php
        since the html tags will be put together by parsing the results object
        */

        // the final request with 
        // valid query term and valid token
        $results = $api->search($query, $type, $options);
        $results->{'mytoken'} = $newToken;
        $total = $results->artists->total;
        $searchtermTimestamp = time();

        // store searchterm in db
        $searchTermHandler = new \App\SearchtermHandler();
        $storeTerm = $searchTermHandler->storeSearchterms($currentSearchTerm, $total, $searchtermTimestamp);
        // var_dump($total);

        return $results;
    }
}
