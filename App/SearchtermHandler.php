<?php

namespace App;

class SearchtermHandler
{

    public function storeSearchterms($currentSearchTerm, $total, $searchtermTimestamp)
    {

        // check if searchterm already exists
        $stm = new \Models\SearchtermModel();
        $getSearchterm = $stm->getSearchterm($currentSearchTerm);

        // if the id of the searchterm is bigger than 0 we know we already have this term stored in db
        if ($getSearchterm[0]['searchterm_id'] > 0) {
            // we already have this term in db | so let´s use it
            $currentSearchTerm = $currentSearchTerm;
            $searchTermId = $getSearchterm[0]['searchterm_id'];
        } else {
            // otherwise store if it´s a new term
            $userSearchTerm = $stm->storeSearchterm($currentSearchTerm, $total, $searchtermTimestamp);
            $currentSearchTerm = $currentSearchTerm;

            // and we need the id of this last strored searchterm
            $searchTermId = $stm->lastInsertedSearchtermId();
        }

        // we can save now to user´s db
        // we need to know first which userId this searchterms belongs to
        $userId = $_SESSION['userId'][0]['user_id'];

        // ready to store searchterm
        $userSearchTermstored = $stm->storeUserSearchterm(intval($userId), intval($searchTermId), $total, $searchtermTimestamp);
    }
}
