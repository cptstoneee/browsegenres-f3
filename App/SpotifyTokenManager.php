<?php

namespace App;

use \SpotifyWebAPI;
use \Session;
use App\SystemHelper as SH;

class SpotifyTokenManager
{

    // public function checkIfThereIsATokenInDB()
    // {
    //     $tokenExistsInDB = [];
    //     $tokenExistsInDB = new Models\TokenModel();
    //     $tokenExistsInDB->checkTokenExistence();

    //     return $tokenExistsInDB;
    // }


    /* 
    
    */
    // public function checkIfSavedTokenIsStillValid(): bool
    // {
    //     // get current timestamp
    //     $getTime = time();

    //     // get timestamp from saved token
    //     $expirationTimefromSavedToken = new Models\TokenModel();

    //     if ($getTime < $expirationTimefromSavedToken) {
    //         $savedTokenIsStillValid = true;
    //     } else {
    //         $savedTokenIsStillValid = false;
    //     }
    //     return $savedTokenIsStillValid;
    // }



    /* 
    1. request a valid token from spotify passing the client ID (=admin) credentials
    2. aks current and add 3500 sec for expiration time
    3. save to db, and
    4. get the id of this (last)inserted token; this is important so you know which token to choose when querying it
    5. return id and token
    */
    public function requestNewToken()
    {
        $session = new SpotifyWebAPI\Session(
            '309e69f9768341b4b4165121571e418c', //'client_ID',
            'f7b1090a9f0e4ff9835fb43d50369197' //'client_SECRET'
        );
        $session->requestCredentialsToken();
        $accessToken = $session->getAccessToken();
        $_SESSION['token'] = $accessToken;
        $this->$_SESSION['token'] = $accessToken;

        $currentTime = time();
        $tokenExpirationTime = $currentTime + 3500;

        $tokenmodel = new \Models\TokenModel();
        $tokenmodel->storeToken($accessToken, $tokenExpirationTime);
        $getLastTokenId = $tokenmodel->lastInsertedTokenId();
        $hereIsYourNewToken = $tokenmodel->getToken($getLastTokenId);
        return array($getLastTokenId, $hereIsYourNewToken);
    }

    // public function updateToken(){

    //     $session = new SpotifyWebAPI\Session(
    //         '309e69f9768341b4b4165121571e418c', //'client_ID',
    //         'f7b1090a9f0e4ff9835fb43d50369197' //'client_SECRET'
    //     );
    //     $session->requestCredentialsToken();
    //     $accessToken = $session->getAccessToken();
    //     $_SESSION['token'] = $accessToken;
    //     $this->$_SESSION['token'] = $accessToken;

    //     $currentTime = time();
    //     $tokenExpirationTime = $currentTime + 3500;
    //     $updateToken = new Models\TokenModel();
    //     $tokenIsUpdated = $updateToken->updateToken($accessToken, $tokenExpirationTime);
    //     var_dump($tokenIsUpdated);

    // }


    /* 
    we ask the current time and ask db if there´s any token with expiration (=timestamp when stored + 3500 seconds [since spotify token is only valid for 60 minutes]) time bigger than the 
    this will either return a (still) valid token or just null
    */
    public function getLastSavedToken()
    {
        $currentTime = time();
        $getToken = new \Models\TokenModel();
        $validToken = $getToken->getToken($currentTime);
        return $validToken;
    }
}
